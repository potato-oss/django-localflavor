# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

# see https://en.wikipedia.org/wiki/ISO_3166-2:TH
AREA_CHOICES = (
    ('TH-10', _('Bangkok')),  # metropolitan administration
    ('TH-S', _('Phatthaya')),  # special administrative city
    ('TH-37', _('Amnat Charoen')),  # province
    ('TH-15', _('Ang Thong')),  # province
    ('TH-38', _('Bueng Kan')),  # province
    ('TH-31', _('Buri Ram')),  # province
    ('TH-24', _('Chachoengsao')),  # province
    ('TH-18', _('Chai Nat')),  # province
    ('TH-36', _('Chaiyaphum')),  # province
    ('TH-22', _('Chanthaburi')),  # province
    ('TH-50', _('Chiang Mai')),  # province
    ('TH-57', _('Chiang Rai')),  # province
    ('TH-20', _('Chon Buri')),  # province
    ('TH-86', _('Chumphon')),  # province
    ('TH-46', _('Kalasin')),  # province
    ('TH-62', _('Kamphaeng Phet')),  # province
    ('TH-71', _('Kanchanaburi')),  # province
    ('TH-40', _('Khon Kaen')),  # province
    ('TH-81', _('Krabi')),  # province
    ('TH-52', _('Lampang')),  # province
    ('TH-51', _('Lamphun')),  # province
    ('TH-42', _('Loei')),  # province
    ('TH-16', _('Lop Buri')),  # province
    ('TH-58', _('Mae Hong Son')),  # province
    ('TH-44', _('Maha Sarakham')),  # province
    ('TH-49', _('Mukdahan')),  # province
    ('TH-26', _('Nakhon Nayok')),  # province
    ('TH-73', _('Nakhon Pathom')),  # province
    ('TH-48', _('Nakhon Phanom')),  # province
    ('TH-30', _('Nakhon Ratchasima')),  # province
    ('TH-60', _('Nakhon Sawan')),  # province
    ('TH-80', _('Nakhon Si Thammarat')),  # province
    ('TH-55', _('Nan')),  # province
    ('TH-96', _('Narathiwat')),  # province
    ('TH-39', _('Nong Bua Lam Phu')),  # province
    ('TH-43', _('Nong Khai')),  # province
    ('TH-12', _('Nonthaburi')),  # province
    ('TH-13', _('Pathum Thani')),  # province
    ('TH-94', _('Pattani')),  # province
    ('TH-82', _('Phangnga')),  # province
    ('TH-93', _('Phatthalung')),  # province
    ('TH-56', _('Phayao')),  # province
    ('TH-67', _('Phetchabun')),  # province
    ('TH-76', _('Phetchaburi')),  # province
    ('TH-66', _('Phichit')),  # province
    ('TH-65', _('Phitsanulok')),  # province
    ('TH-54', _('Phrae')),  # province
    ('TH-14', _('Phra Nakhon Si Ayutthaya')),  # province
    ('TH-83', _('Phuket')),  # province
    ('TH-25', _('Prachin Buri')),  # province
    ('TH-77', _('Prachuap Khiri Khan')),  # province
    ('TH-85', _('Ranong')),  # province
    ('TH-70', _('Ratchaburi')),  # province
    ('TH-21', _('Rayong')),  # province
    ('TH-45', _('Roi Et')),  # province
    ('TH-27', _('Sa Kaeo')),  # province
    ('TH-47', _('Sakon Nakhon')),  # province
    ('TH-11', _('Samut Prakan')),  # province
    ('TH-74', _('Samut Sakhon')),  # province
    ('TH-75', _('Samut Songkhram')),  # province
    ('TH-19', _('Saraburi')),  # province
    ('TH-91', _('Satun')),  # province
    ('TH-17', _('Sing Buri')),  # province
    ('TH-33', _('Si Sa Ket')),  # province
    ('TH-90', _('Songkhla')),  # province
    ('TH-64', _('Sukhothai')),  # province
    ('TH-72', _('Suphan Buri')),  # province
    ('TH-84', _('Surat Thani')),  # province
    ('TH-32', _('Surin')),  # province
    ('TH-63', _('Tak')),  # province
    ('TH-92', _('Trang')),  # province
    ('TH-23', _('Trat')),  # province
    ('TH-34', _('Ubon Ratchathani')),  # province
    ('TH-41', _('Udon Thani')),  # province
    ('TH-61', _('Uthai Thani')),  # province
    ('TH-53', _('Uttaradit')),  # province
    ('TH-95', _('Yala')),  # province
    ('TH-35', _('Yasothon')),  # province
)
