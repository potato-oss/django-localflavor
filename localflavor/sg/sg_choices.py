# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

# see https://en.wikipedia.org/wiki/ISO_3166-2:SG
SUBDIVISION_CHOICES = (
    ('SG-01', _('Central Singapore')),
    ('SG-02', _('North East')),
    ('SG-03', _('North West')),
    ('SG-04', _('South East')),
    ('SG-05', _('South West')),
)
