# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

# see https://en.wikipedia.org/wiki/ISO_3166-2:PH
STATE_CHOICES = (
    ('PH-14', _('Autonomous Region in Muslim Mindanao Autonomous Region in Muslim Mindanao (ARMM)')),
    ('PH-05', _('Bicol Bicol (Region V)')),
    ('PH-02', _('Cagayan Valley Cagayan Valley (Region II)')),
    ('PH-40', _('Calabarzon Calabarzon (Region IV-A)')),
    ('PH-13', _('Caraga Caraga (Region XIII)')),
    ('PH-03', _('Central Luzon Central Luzon (Region III)')),
    ('PH-07', _('Central Visayas Central Visayas (Region VII)')),
    ('PH-15', _('Cordillera Administrative Region Cordillera Administrative Region (CAR)')),
    ('PH-11', _('Davao Davao (Region XI)')),
    ('PH-08', _('Eastern Visayas Eastern Visayas (Region VIII)')),
    ('PH-01', _('Ilocos Ilocos (Region I)')),
    ('PH-41', _('Mimaropa Mimaropa (Region IV-B)')),
    ('PH-00', _('National Capital Region National Capital Region')),
    ('PH-10', _('Northern Mindanao Northern Mindanao (Region X)')),
    ('PH-12', _('Soccsksargen Soccsksargen (Region XII)')),
    ('PH-06', _('Western Visayas Western Visayas (Region VI)')),
    ('PH-09', _('Zamboanga Peninsula Zamboanga Peninsula (Region IX)'))
)
