# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

# see https://en.wikipedia.org/wiki/ISO_3166-2:VN
AREA_CHOICES = (
    ('VN-44', _(u'An Giang')),  # province
    ('VN-43', _(u'Bà Rịa–Vũng Tàu')),  # province
    ('VN-54', _(u'Bắc Giang')),  # province
    ('VN-53', _(u'Bắc Kạn')),  # province
    ('VN-55', _(u'Bạc Liêu')),  # province
    ('VN-56', _(u'Bắc Ninh')),  # province
    ('VN-50', _(u'Bến Tre')),  # province
    ('VN-31', _(u'Bình Định')),  # province
    ('VN-57', _(u'Bình Dương')),  # province
    ('VN-58', _(u'Bình Phước')),  # province
    ('VN-40', _(u'Bình Thuận')),  # province
    ('VN-59', _(u'Cà Mau')),  # province
    ('VN-04', _(u'Cao Bằng')),  # province
    ('VN-33', _(u'Đắk Lắk')),  # province
    ('VN-72', _(u'Đắk Nông')),  # province
    ('VN-71', _(u'Điện Biên')),  # province
    ('VN-39', _(u'Đồng Nai')),  # province
    ('VN-45', _(u'Đồng Tháp')),  # province
    ('VN-30', _(u'Gia Lai')),  # province
    ('VN-03', _(u'Hà Giang')),  # province
    ('VN-63', _(u'Hà Nam')),  # province
    ('VN-23', _(u'Hà Tĩnh')),  # province
    ('VN-61', _(u'Hải Dương')),  # province
    ('VN-73', _(u'Hậu Giang')),  # province
    ('VN-14', _(u'Hòa Bình')),  # province
    ('VN-66', _(u'Hưng Yên')),  # province
    ('VN-34', _(u'Khánh Hòa')),  # province
    ('VN-47', _(u'Kiên Giang')),  # province
    ('VN-28', _(u'Kon Tum')),  # province
    ('VN-01', _(u'Lai Châu')),  # province
    ('VN-35', _(u'Lâm Đồng')),  # province
    ('VN-09', _(u'Lạng Sơn')),  # province
    ('VN-02', _(u'Lào Cai')),  # province
    ('VN-41', _(u'Long An')),  # province
    ('VN-67', _(u'Nam Định')),  # province
    ('VN-22', _(u'Nghệ An')),  # province
    ('VN-18', _(u'Ninh Bình')),  # province
    ('VN-36', _(u'Ninh Thuận')),  # province
    ('VN-68', _(u'Phú Thọ')),  # province
    ('VN-32', _(u'Phú Yên')),  # province
    ('VN-24', _(u'Quảng Bình')),  # province
    ('VN-27', _(u'Quảng Nam')),  # province
    ('VN-29', _(u'Quảng Ngãi')),  # province
    ('VN-13', _(u'Quảng Ninh')),  # province
    ('VN-25', _(u'Quảng Trị')),  # province
    ('VN-52', _(u'Sóc Trăng')),  # province
    ('VN-05', _(u'Sơn La')),  # province
    ('VN-37', _(u'Tây Ninh')),  # province
    ('VN-20', _(u'Thái Bình')),  # province
    ('VN-69', _(u'Thái Nguyên')),  # province
    ('VN-21', _(u'Thanh Hóa')),  # province
    ('VN-26', _(u'Thừa Thiên–Huế')),  # province
    ('VN-46', _(u'Tiền Giang')),  # province
    ('VN-51', _(u'Trà Vinh')),  # province
    ('VN-07', _(u'Tuyên Quang')),  # province
    ('VN-49', _(u'Vĩnh Long')),  # province
    ('VN-70', _(u'Vĩnh Phúc')),  # province
    ('VN-06', _(u'Yên Bái')),  # province
    ('VN-CT', _(u'Cần Thơ')),  # municipality
    ('VN-DN', _(u'Đà Nẵng')),  # municipality
    ('VN-HN', _(u'Hà Nội')),  # municipality
    ('VN-HP', _(u'Hải Phòng')),  # municipality
    ('VN-SG', _(u'Hồ Chí Minh')),  # municipality
)
